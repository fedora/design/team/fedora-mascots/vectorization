# Vectorization

This is a project to create vectorized versions of the Fedora mascot artwork.

## Background

Lauryn Dake, a very talented character design artist, came up with character design artwork for three Fedora mascots: Panda, Beefy Miracle, and Badger. She used a tool called Clip Studio Paint to create the artwork, but unfortunately, we could not figure out how to export or convert the native Clip Studio Paint format to a vector-based format (even though internally, in the tool, it appeared to be vector-based.) We do not have the Clip Studio Paint source files, only bitmap exports of the artwork at a high-resolution. 

We need this artwork in vector format so we can adapt and expand the artwork more easily. We would like these redrawn in Inkscape as vectors and saved out in SVG format. Ideally, the vectors would be created and grouped with labels in such a way that the various body parts can be rigged / puppeted if need be to slightly change the poses. We would also like the vectors uplaoded into Penpot libraries.

## Steps

- [ ] [Vectorize the Fedora Panda artwork](https://gitlab.com/fedora/design/fedora-mascots/vectorization/-/issues/1)
- [ ] [Vectorize the Beefy Miracle artwork](https://gitlab.com/fedora/design/fedora-mascots/vectorization/-/issues/2)
- [ ] [Vectorize the Fedora Badger artwork](https://gitlab.com/fedora/design/fedora-mascots/vectorization/-/issues/3)
- [ ] Save the Fedora Panda vectors to Fedora mascots GitLab git repo
- [ ] Upload the Beefy Miracle vectors to Fedora mascots GitLab git repo
- [ ] Upload the Fedora Badger vectors to Fedora mascots GitLab git repo
- [ ] Upload the Fedora Panda vectors to Penpot character library
- [ ] Upload the Beefy Miracle vectors to Penpot character library
- [ ] Upload the Fedora Badger vectors to Penpot character library

## Resources

You will want to taper the strokes in the vector versions of the artwork to match how the lines are tapered in the bitmap source artwork. Here is a good tutorial for achieving that effect - it is quite thorough, it's roughly 8 minutes long:

[Taper lines using powerstroke Inkscape tutorial](https://www.youtube.com/watch?v=TKHOm-EQsA4) (YouTube, 8:24)

## Contributing

We are looking for designers with experience / skill in creating vector artwork who:

- Have signed the [Fedora Project Contributor Agreement](https://fedoraproject.org/wiki/Legal:Fedora_Project_Contributor_Agreement) 
- Agree to abide by the [Fedora Community Code of Conduct](https://docs.fedoraproject.org/en-US/project/code-of-conduct/).

Your contact for this project for help / guidance / etc. is Máirín Duffy ([@duffy:fedora.im](https://matrix.to/#/@duffy:fedora.im)). You are also invited and encouraged to join us in the Fedora Design team chat at [#design:fedoraproject.org](https://matrix.to/#/#design:fedoraproject.org).
